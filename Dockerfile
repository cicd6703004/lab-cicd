FROM openjdk:17-jdk-slim

LABEL maintainer="Tidiany Toure tourecheikhmame@gmail.com"

EXPOSE 8080

ADD target/lab-cicd.jar lab-cicd.jar

ENTRYPOINT ["java", "-jar", "lab-cicd.jar"]